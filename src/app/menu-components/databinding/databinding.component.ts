import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-databinding',
  templateUrl: './databinding.component.html',
  styleUrls: ['./databinding.component.css']
})
export class DatabindingComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  readonly steam: string = "https://store.steampowered.com/?l=portuguese";
  readonly nome: string = "Rúben Santos";

  nome2: string;
  idade: number;
  n1: number;
  n2: number;
  resultado: number;
  imagem: string = "https://d2bgjx2gb489de.cloudfront.net/gbb-blogs/wp-content/uploads/2020/01/17113513/Porto-shot-of-the-city.jpg"
  mouseover: boolean = false;
  frase: string;
  logs: string[] = [];

  getValor = () => { 
    this.resultado = this.n1 + this.n2;
   };

  setNome = (nome) => { 
    this.nome2 = nome;
  }

  setIdade = (idade) => {
    this.idade = idade;
  }

  mouseOverOrOut = () => {
    this.mouseover = !this.mouseover;
  }

  onMudouValor(evento){
    this.logs.push(evento);
  }

  
   cores: object[] = [
    { ViewValue: "Vermelho", value: "red"},
    { ViewValue: "Azul", value: "blue"},
    { ViewValue: "Amarelo", value: "yellow"},
    { ViewValue: "Verde", value: "green"},
    { ViewValue: "Laranja", value: "orange"}
   ];


}
