import { Component, OnInit, Input, Output } from '@angular/core';

@Component({
  selector: 'app-input-component',
  templateUrl: './input-component.component.html',
  styleUrls: ['./input-component.component.css']
})
export class InputComponent implements OnInit {

  @Input('frase') fraseInput: string

  constructor() { }

  ngOnInit(): void {
  }

}
