import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';

@Component({
  selector: 'app-output-component',
  templateUrl: './output-component.component.html',
  styleUrls: ['./output-component.component.css']
})
export class OutputComponent implements OnInit {

  @Input('valor') value: number
  @Output() mudouValor = new EventEmitter()

  incrementa = () => {
    this.value++; 
    this.mudouValor.emit(`O valor subiu para ${this.value}`)
  }
  decrementa = () => { 
    this.value--;
    this.mudouValor.emit(`O valor desceu para ${this.value}`);
  }

  constructor(){}

  
 
  ngOnInit(): void {
  }

}
