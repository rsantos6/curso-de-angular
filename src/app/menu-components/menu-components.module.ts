import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatTabsModule } from '@angular/material/tabs';
import { FormsModule } from '@angular/forms';
import { MatSelectModule } from '@angular/material/select';
import { MatButtonModule } from '@angular/material/button';



import { IntroducaoComponent } from './introducao/introducao.component';
import { DatabindingComponent } from './databinding/databinding.component';
import { AngularcliComponent } from './angularcli/angularcli.component';
import { DiretivasComponent } from './diretivas/diretivas.component';
import { ServicosComponent } from './servicos/servicos.component';
import { PipesComponent } from './pipes/pipes.component';
import { RotasComponent } from './rotas/rotas.component';
import { FormulariosTemplateDrivenComponent } from './formularios-template-driven/formularios-template-driven.component';
import { FormulariosReativosDatadrivenComponent } from './formularios-reativos-datadriven/formularios-reativos-datadriven.component';
import { HttpComponent } from './http/http.component';
import { DepuracaoDebugComponent } from './depuracao-debug/depuracao-debug.component';
import { BuildEDeployComponent } from './build-e-deploy/build-e-deploy.component';
import { InputComponent } from './databinding/input-component/input-component.component';
import { OutputComponent } from './databinding/output-component/output-component.component';


@NgModule({
  declarations: [
    IntroducaoComponent,
    DatabindingComponent,
    AngularcliComponent,
    DiretivasComponent,
    ServicosComponent,
    PipesComponent,
    RotasComponent,
    FormulariosTemplateDrivenComponent,
    FormulariosReativosDatadrivenComponent,
    HttpComponent,
    DepuracaoDebugComponent,
    BuildEDeployComponent,
    InputComponent,
    OutputComponent
  ],
  imports: [
    CommonModule,
    MatTabsModule,
    FormsModule,
    MatSelectModule,
    MatButtonModule
  ]
})
export class MenuComponentsModule { }
