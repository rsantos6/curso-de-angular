import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BuildEDeployComponent } from './build-e-deploy.component';

describe('BuildEDeployComponent', () => {
  let component: BuildEDeployComponent;
  let fixture: ComponentFixture<BuildEDeployComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BuildEDeployComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BuildEDeployComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
