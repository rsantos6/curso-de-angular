import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormulariosReativosDatadrivenComponent } from './formularios-reativos-datadriven.component';

describe('FormulariosReativosDatadrivenComponent', () => {
  let component: FormulariosReativosDatadrivenComponent;
  let fixture: ComponentFixture<FormulariosReativosDatadrivenComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormulariosReativosDatadrivenComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormulariosReativosDatadrivenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
