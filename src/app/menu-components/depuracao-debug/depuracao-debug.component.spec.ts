import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DepuracaoDebugComponent } from './depuracao-debug.component';

describe('DepuracaoDebugComponent', () => {
  let component: DepuracaoDebugComponent;
  let fixture: ComponentFixture<DepuracaoDebugComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DepuracaoDebugComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DepuracaoDebugComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
