import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormulariosTemplateDrivenComponent } from './formularios-template-driven.component';

describe('FormulariosTemplateDrivenComponent', () => {
  let component: FormulariosTemplateDrivenComponent;
  let fixture: ComponentFixture<FormulariosTemplateDrivenComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormulariosTemplateDrivenComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormulariosTemplateDrivenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
