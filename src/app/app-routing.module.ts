import { BuildEDeployComponent } from './menu-components/build-e-deploy/build-e-deploy.component';
import { DepuracaoDebugComponent } from './menu-components/depuracao-debug/depuracao-debug.component';
import { HttpComponent } from './menu-components/http/http.component';
import { FormulariosReativosDatadrivenComponent } from './menu-components/formularios-reativos-datadriven/formularios-reativos-datadriven.component';
import { FormulariosTemplateDrivenComponent } from './menu-components/formularios-template-driven/formularios-template-driven.component';
import { RotasComponent } from './menu-components/rotas/rotas.component';
import { PipesComponent } from './menu-components/pipes/pipes.component';
import { ServicosComponent } from './menu-components/servicos/servicos.component';
import { DiretivasComponent } from './menu-components/diretivas/diretivas.component';
import { AngularcliComponent } from './menu-components/angularcli/angularcli.component';
import { DatabindingComponent } from './menu-components/databinding/databinding.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { IntroducaoComponent } from './menu-components/introducao/introducao.component';

const routes: Routes = [
  { path: 'introducao', component: IntroducaoComponent },
  { path: 'databinding', component: DatabindingComponent },
  { path: 'angularcli', component: AngularcliComponent },
  { path: 'diretivas', component: DiretivasComponent },
  { path: 'servicos', component: ServicosComponent },
  { path: 'pipes', component: PipesComponent },
  { path: 'rotas', component: RotasComponent },
  { path: 'formularios-templatedriven', component: FormulariosTemplateDrivenComponent },
  { path: 'formularios-reativos-datadriven', component: FormulariosReativosDatadrivenComponent },
  { path: 'http', component: HttpComponent },
  { path: 'depuracao-debug', component: DepuracaoDebugComponent },
  { path: 'build-e-deploy', component: BuildEDeployComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
